package main

import (
	"flag"
	"fmt"
	"os"
	"time"
)

type LogLine struct {
	TimeStamp time.Time
	Text      string
	Filename  string
}

func main() {
	startStringRef := flag.String("start", "2020-06-10T13:20:00.000", "The start time to look for in logs")
	endStringRef := flag.String("end", "", "The end time to look for in logs")
	durationStringRef := flag.String("d", "10m5s", "Duration to look for in logs")

	flag.Parse()

	start, err := time.Parse("2006-01-02T15:04:05", *startStringRef)
	if err != nil {
		fmt.Println("Malformed start time:", err)
		os.Exit(-1)
	}

	var end time.Time

	if *endStringRef == "" {
		duration, err := time.ParseDuration(*durationStringRef)
		if err != nil {
			fmt.Println("Malformed duration time:", err)
			os.Exit(-1)
		}
		end = start.Add(duration)
	} else {
		end, err = time.Parse("2006-01-02T15:04:05", *endStringRef)
		if err != nil {
			fmt.Println("Malformed end time:", err)
			os.Exit(-1)
		}
	}

	fmt.Println("start: ", start)
	fmt.Println("end: ", end)

	result1, err1 := cluster_parse("C:\\Users\\husztad\\Go_training\\homework1\\clustercontroller.log", start, end)
	fmt.Println(result1)
	fmt.Println(err1)
	result2, err2 := active_parse("C:\\Users\\husztad\\Go_training\\homework1\\activemq.log", start, end)
	fmt.Println(result2)
	fmt.Println(err2)
	result3, err3 := gateway_parse("C:\\Users\\husztad\\Go_training\\homework1\\gateway.log", start, end)
	fmt.Println(result3)
	fmt.Println(err3)
}
