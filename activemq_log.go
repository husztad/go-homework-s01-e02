package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"
)

var activeLogLineRe = regexp.MustCompile(`^(\d+-\d+-\d+ \d+:\d+:\d+,\d+)`)

func activeParseLine(line string, start, end time.Time) *LogLine {
	result := activeLogLineRe.FindSubmatch([]byte(line))
	// if we have errors
	if result == nil {
		return nil
	}
	r := strings.Replace(string(result[0]), ",", ".", -1)

	// parse the time
	parsedTime, err := time.Parse("2006-01-02 15:04:05.000", r)
	if err != nil {
		// fmt.Println(err)
		return nil
	}
	if parsedTime.After(start) && parsedTime.Before(end) {
		return &LogLine{
			TimeStamp: parsedTime,
			Text:      line}
	}

	return nil
}

func active_parse(path string, start, end time.Time) ([]LogLine, error) {
	// storage for output
	logLinesMatched := make([]LogLine, 0)

	file, err := os.Open(path)
	if err != nil {
		return logLinesMatched, fmt.Errorf("while opening '%v' for reading: %v", path, err)
	}

	// close the file
	defer file.Close()
	// read line-by-line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// read line
		lineText := scanner.Text()
		p := activeParseLine(lineText, start, end)

		if p != nil {
			logLinesMatched = append(logLinesMatched, *p)
		}
	}
	return logLinesMatched, nil
}
