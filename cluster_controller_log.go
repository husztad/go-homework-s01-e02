package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"time"
)

var clusterLogLineRe = regexp.MustCompile(`^(\d+-\d+-\d+ \d+:\d+:\d+.\d+ \+\d+)`)

func clusterParseLine(line string, start, end time.Time) *LogLine {
	result := clusterLogLineRe.FindSubmatch([]byte(line))
	// if we have errors
	if result == nil {
		return nil
	}

	// parse the time
	parsedTime, err := time.Parse("2006-01-02 15:04:05.000 +0000", string(result[0]))
	if err != nil {
		return nil
	}
	if parsedTime.After(start) && parsedTime.Before(end) {
		return &LogLine{
			TimeStamp: parsedTime,
			Text:      line}
	}

	return nil
}

func cluster_parse(path string, start, end time.Time) ([]LogLine, error) {
	// storage for output
	logLinesMatched := make([]LogLine, 0)

	file, err := os.Open(path)
	if err != nil {
		return logLinesMatched, fmt.Errorf("while opening '%v' for reading: %v", path, err)
	}

	// close the file
	defer file.Close()
	// read line-by-line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// read line
		lineText := scanner.Text()
		p := clusterParseLine(lineText, start, end)

		if p != nil {
			logLinesMatched = append(logLinesMatched, *p)
		}
	}
	return logLinesMatched, nil
}
